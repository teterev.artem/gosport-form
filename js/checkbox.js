$(document).ready(function () {
  $('.hidden_checkbox-main.hidden_checkbox').on('click', function () {
    var $this = $(this),
      innerCheck = $this.closest('.main_checkbox').siblings('.checkbox_group').find('input');


    if ($this.prop('checked')) {
      innerCheck.prop('checked', true);
      console.log('true');
    } else {
      innerCheck.prop('checked', false);
      console.log('false');
    }

  });
  $('.inner_checkbox').on('click', function () {
    var $this = $(this);
    var ch = $this.closest('.form_group-filter').find('.inner_checkbox');
    var allCh = [];
    for (var i = 0; i < ch.length; i++) {
      if ($(ch[i]).find('.hidden_checkbox').prop('checked')) {
        allCh.push(true);
      }
    }


    if (allCh.length === ch.length) {
      console.log(true);
      $this.closest('.form_group-filter').find('.hidden_checkbox-main').prop('checked', true);
    } else {
      $this.closest('.form_group-filter').find('.hidden_checkbox-main').prop('checked', false);
    }
  });
});
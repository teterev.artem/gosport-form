$(document).ready(function(){

  var point = $('.range_point');

  point.on('mousedown', function(e){
    var $this = $(this),
        container = $this.closest('.fake_range'),
        containerWidth = container.width(),
        shiftX = e.pageX - $this.width() / 2,//точное положение мышки на элементе
        posX = e.pageX - shiftX - $this.position().left + $this.offset().left,//смещение влево
        distanceMax = container.siblings('.range_distance').find('.start_distance-max'),//значение выводимое на экран
        rangeProgress = container.find('.range_progress'),
        range = container.siblings('.hidden_range'),
        rangeMax = range.attr('max'), //максимальное значение input range
        rangeMin = range.attr('min'); //минимальное значение input range

    $(document).on('mousemove', function(e){
      var currentPos = (e.pageX - posX) * 100 / containerWidth; //текущая позиция бегунка
      var currentRange = rangeMax * Math.floor(currentPos) / 100;
      
      //текущая позиция input range
      point.css('left', currentPos - $this.position().left);
      point.css('left', Math.floor(currentPos) + '%');
      rangeProgress.css('width', Math.floor(currentPos) + '%');
      range.val(currentRange);

    // проверка условия крайних значений
        function checkVal(max, min){
          if (currentRange > max){
            distanceMax.text(max + 'km');
            currentRange = max;
          }else if (currentRange < min){
            distanceMax.text(min + 'km');
            currentRange = min;
          }else{
            distanceMax.text(currentRange + 'km');
            currentRange = currentRange;
          };
        };

      checkVal(rangeMax, rangeMin);   

      
      //функция задает максимальный и мимнимальный порог бегунка
       function moveAt(max, min){
         if(currentPos >= max){
           point.css('left', max + '%');
           rangeProgress.css('width', max + '%')
         };
         if(currentPos <= min){
           point.css('left', min + '%');
           rangeProgress.css('width', min + '%');
         }
       };

      //условие крайних положений бегунка
      moveAt(100, 0);



    
    })
    $(document).on('mouseup', function(){
      $(document).off('mousemove');      
    });
  });
});  